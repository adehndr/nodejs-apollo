const { gql } = require('apollo-server-express');

const types = `
    type Response {
        response : ResponseMsgSts
        info : [ResponseDetail]
    }
    type ResponseMsgSts {
        errMsg : String,
        status : String
    }
    type ResponseDetail {
        ship_id : String,
        pesan : String,
        banyak : String,
        reason : String,
        cancelmeaning : String,
        customer_po_number: String,
        orig_sys_document_ref : String,
        creation_date : String,
        ORACLE_SO_NUMBER : String,
        username : String,
        KODEPROD : String,
        PRODNAME : String,
        PAYMENTTYPE : String
    }
    `;
const queries = `GetStockOut(shipid : String) : Response`;
const typeDefs = gql`
    ${types}
    type Query {${queries}}
    `;

module.exports = typeDefs;