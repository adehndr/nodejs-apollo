const _ = require('lodash');

const resolvers = {
    Query : {
        GetStockOut : async (_,shipId = {shipId},{dataSources}) => {
            return await dataSources.stockOutDataSource.getStockOut(shipId);
        }

    }
};
module.exports = resolvers;