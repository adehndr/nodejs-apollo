const StockOutDataSource = require('./StockOutDataSource');

const DataSource = () => ({
    stockOutDataSource : new StockOutDataSource()
});

module.exports = DataSource;