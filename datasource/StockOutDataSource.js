const { RESTDataSource } = require('apollo-datasource-rest');
const { json } = require('body-parser');
const _ = require('lodash');

class StockOut extends RESTDataSource{
    constructor(){
        super();
        this.baseURL = "https://emosservice2.enseval.com/PRODListStockOut.aspx";
    }
    async getStockOut(shipId){
        if(_.isEmpty(shipId)){
            const resultEmpty = `{"response" : {"errMsg" : "Data Tidak Ada", "status" : "554"}, "info" : []}`;
            return JSON.parse(resultEmpty);
        }
        const result = await this.get('',`shipid=${shipId.shipid}`);
        var tempResult = result;
        tempResult = tempResult.slice(19);
        tempResult = tempResult.slice(0,-10);
        if(tempResult.includes("Tidak Ada Data")) 
        {   const resultEmpty = `{"response" : {"errMsg" : "Data Tidak Ada", "status" : "554"}, "info" : []}`;
            return JSON.parse(resultEmpty);
        }
        tempResult = `{"response" : {"errMsg" : "", "status" : "0"}, ${tempResult}}`;
        tempResult = JSON.parse(tempResult);
        return tempResult;
    }

    getNoStockOut(){
        const resultEmpty = `{"response" : {"errMsg" : "Data Tidak Ada", "status" : "554"}, "info" : []}`;
        return JSON.parse(resultEmpty);
    }
}
module.exports = StockOut;