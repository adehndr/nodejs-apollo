const {ApolloServer} = require('apollo-server-express');
const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core');
const express = require('express');
const typeDefs = require('./schema/Schema');
const resolvers = require('./resolver/Resolver');
const dataSources = require('./datasource/DataSource');

const app = express();
const port = process.env.PORT || 5000;

let apolloServer = null;
async function startServer() {
  apolloServer = new ApolloServer({
    typeDefs,resolvers,
    dataSources : dataSources,
    plugins: [
        ApolloServerPluginLandingPageGraphQLPlayground({
          // options
        })
      ], 
    introspection:true
});
  await apolloServer.start();
  apolloServer.applyMiddleware({ app });
}

startServer();
app.get('/',(req,res) => {
  res.send("Welcome");
});
app.listen({port: port}, () => {
  console.log(`server running on port ${port}`);
  console.log(`gql path is http://localhost:${port}${apolloServer.graphqlPath}`);
});


/*   app.get('/',(req,res)=> {
    res.send("<p>Connect Succes</p>");
});

app.use((req,res) => {
    res.status(404).send("<p>Error 404</p> <p>Url Error</p>");
});

app.listen(port,() => {
    console.log(`Succesfully connected at port : ${port}`);
});
 */